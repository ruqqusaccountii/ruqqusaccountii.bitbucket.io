var rgp = [2,2];
var rga = false;
var rgs = [25, 25];
var rgps = 2;
var winTimer = -1;

RL_SIZE = [25, 25]
NONBLOCKING = ['.',':','^','X','x', '-'];
IMPORTANT = ['+', 'X'];
class Roguelike {
	constructor(source, map, objs) {
		// 2D array of tiles. Each background tile is:
		// '.': empty
		// ':': sliding surface
		// '#': barrier. Blocks motion
		// '1-9': tile identifier. Opens associated door when matched.
		// '+': door; closed. '-' when open.
		// '^': exfiltration point.
		// 'X': target. 'x' when reached.
		// 
		// Foreground objects:
		// '@': Player
		// 'V': Threat. Chases player. If it catches player, player moves to nearest exit.
		// '1-9': Tile.
		this.door_dict = {}; // Maps door numbers to door locations
		this.field = [];
		for (let y = 0; y<RL_SIZE[1]; y++) {
			let column = [];
			for (let x = 0; x<RL_SIZE[0]; x++) {
				column.push(['.', null]);
			}
			this.field.push(column);
		}
		this.playerLoc = false;
		this.exfiltration = false;
		this.player_hit = false;
		this.threats = [];
		this.teleporters = []; // Stores coordinates of teleporters, followed by ID
		this.winTimer = 0;
		this.source = source;
		this.build(map, objs.slice());
	}

	build(map, objs) {
		this.field = [];
		for (let y = 0; y<RL_SIZE[1]; y++) {
			let column = [];
			let row_contents = map[y];
			for (let x = 0; x<RL_SIZE[0]; x++) {
				let c = row_contents[x];
				if (c != '*') {
					column.push([c, null]);
				} else {
					let content = objs[0].split('*');
					let obj = content[0];
					objs.splice(0, 1);
					if (obj[0]!='-' &&obj[0]!='+' && parseInt(obj)) {
						// Is the object a tile?
						c = (content.length==1)?':':content[1];
						//
						if (this.door_dict[obj[0]] == undefined) {
							this.door_dict[obj[0]] = [];
						}
					} else {
						c = '.';
						if (obj[0]=='+') { // Is the object a door?
							if (this.door_dict[obj[1]]) {
								this.door_dict[obj[1]].push([y,x]);
							} else {
								this.door_dict[obj[1]] = [[y,x]];
							}
							c = obj[0];
							obj = [null];
						} else if (obj[0]=='-') { // Open doors
							if (this.door_dict[obj[1]]) {
								this.door_dict[obj[1]].push([y,x]);
							} else {
								this.door_dict[obj[1]] = [[y,x]];
							}
							c = obj[0];
							obj = [null];
						} else if (obj[0] == 'T') {
							c = 'T';
							this.teleporters.push([y, x, obj.slice(1)]);
							obj = [null];
						} else if (obj == 'V') {
							this.threats.push([y,x]);
						}
					}
					column.push([c, obj[0]]);
				}
			}
			this.field.push(column);
		}
		// Check tiles, toggle doors accordingly.
		for (let y = 0; y<RL_SIZE[1]; y++) {
			for (let x = 0; x<RL_SIZE[0]; x++) {
				let item = this.field[y][x];
				if (item[1] && parseInt(item[1]) && item[1] == item[0]) {
					this.toggleDoors(item[1]);
				}
			}
		}
	}

	teleport(loc) {
		addUniqueMessage('You feel a sensation of leaping across a chasm.');
		// Get teleporter ID at current location
		let tid = 0;
		this.teleporters.forEach(t=>{
			if (t[0] == loc[0] && t[1] == loc[1]) {
				tid = t[2];
			}
		});
		// Get teleporter 
		let found = false;
		this.teleporters.forEach(t=>{
			if (!found && t[2] == tid && (t[0] != loc[0] || t[1] != loc[1])) {
				loc = [t[0], t[1]];
				found = true;
			}
		});
		play(teleportSound);
		return loc;
	}

	enter(loc) {
		if (this.playerLoc) {
			this.getLoc(this.playerLoc)[1] = null;
			this.getLoc(this.exfiltration)[0] = '.';
		}
		this.playerLoc = loc.slice();
		this.exfiltration = loc.slice();
		this.getLoc(this.playerLoc)[1] = '@';
		this.getLoc(this.exfiltration)[0] = '^';
		this.player_hit = false;
	}

	getLoc(loc) {
		return this.field[loc[0]][loc[1]];
	}

	threatHit() {
		addMessage('You are suddenly overcome with a hot, choking fatigue.');
		this.player_hit = true;
	}

	exfiltrate() {
		this.winTimer = 1;
		if (this.player_hit) {
			addMessage('Half-aware, you clamber back into your vessel. A deep chittering echoes in the back of your mind.');
		} else if (this.source.awake) {
 			addMessage('You return to your ship, as the structure grinds back to life below you.');
		} else {
			addMessage('You return to your ship, to continue your journey.');
		}
	}

	extractPath(prevs, start, dest) {
		// For now, we only want the next step in the path.
		if (mag(sub(start, dest))<2) {
			return dest;
		}
		let prev;
		while (dest[0] != start[0] || dest[1] != start[1]) {
			prev = dest;
			dest = prevs[dest[0]*RL_SIZE[0]+dest[1]];
		}
		return prev;
	}

	getPath(loc, dest) {
		//console.log('START ' + loc + " " + dest);
		let t = this;
		// Standard BFS. We can switch to a* if resources become an issue.
		let moves = [[0,1],[1,0],[0,-1],[-1,0]];
		let queue = [loc];
		let start = loc;
		let prevs = new Array(RL_SIZE[0]*RL_SIZE[1]).fill(false);
		prevs[start[0]*RL_SIZE[0]+start[1]] = true;
		//
		while (queue.length > 0) {
			loc = queue[0];
			queue.splice(0,1);
			let next = [];
			for (let i = 0; i < moves.length; i++) {
				let m = moves[i];
				let to = add(m, loc);
				let c = t.getLoc(to);
				if (to[0] == dest[0] && to[1] == dest[1]) {
					//console.log('set prev for ' + to + ' (' + (to[0]*RL_SIZE[0]+to[1]) + ') to ' + loc)
					prevs[to[0]*RL_SIZE[0]+to[1]] = loc.slice();
					return t.extractPath(prevs, start, dest);
				} else if (c[1]==null && !prevs[to[0]*RL_SIZE[0]+to[1]] && NONBLOCKING.indexOf(c[0]) != -1) {
					queue.push(to);
					//console.log('set prev for ' + to + ' (' + (to[0]*RL_SIZE[0]+to[1]) + ') to ' + loc)
					prevs[to[0]*RL_SIZE[0]+to[1]] = loc.slice();
				}
			}
		}
		return false;
	}

	toggleDoors(num) {
		let t = this;
		let doors = t.door_dict[num];
		doors.forEach(x=>{
			let loc = t.getLoc(x);
			loc[0] = (loc[0]=='-')?'+':'-';
		});
	}

	pushTile(loc, push) {
		let next = add(loc, push);
		let lc = this.getLoc(loc);
		let c = this.getLoc(next);
		if (c[0] != ':' && !parseInt(c[0])) {
			return;
		} else if (c[1] == null) {
			c[1] = lc[1];
			lc[1] = null;
			if (c[0] == c[1] || lc[0] == c[1]) {
				this.toggleDoors(c[1]);
			}
			/* { // Landing on 
				//this.getLoc(this.door_dict[c[1]])[0] = '-';

			} else if (lc[0] == c[1]) {
				//this.getLoc(this.door_dict[c[1]])[0] = '+';
			}*/
			play(pushHackSound);
		} else {
			this.pushTile(next, push);
		}
	}

	move(k) {
		let m = [0,0];
		if (k == W_KEY) {
			m[0] -= 1;
		} else if (k == S_KEY) {
			m[0] += 1;
		} else if (k == A_KEY) {
			m[1] -= 1;
		} else if (k == D_KEY) {
			m[1] += 1;
		} else {
			return;
		}
		this.step(m);
	}

	step(move) {
		let t = this;
		// Move player
		if (t.winTimer) {
			return;
		}
		if (t.player_hit) {
			let path = t.getPath(t.playerLoc, t.exfiltration);
			console.log(path);
			if (path) {
				move = sub(path, t.playerLoc);
			}
		}
		let dest = add(t.playerLoc, move);
		let dest_contents = t.getLoc(dest);
		if (dest_contents[1] == null) {
			// move if possible, do nothing if not.
			if (dest_contents[0] == '^') {
				t.exfiltrate();
				play(beginHackSound);
			} else if (dest_contents[0] == '#') {
				// blocked
			} else if (dest_contents[0] == '+') {
				// blocked
			} else {
				// Non-interrupting tiles
				if (dest_contents[0] == 'X') {
					t.source.awake = false;
					dest_contents[0] = 'x';
					addMessage('The structure is now offline.');
				}
				// Teleporters
				if (dest_contents[0] == 'T') {
					dest = this.teleport(dest);
					dest_contents = t.getLoc(dest);
				}
				//
				let plc = t.getLoc(t.playerLoc);
				dest_contents[1] = plc[1];
				plc[1] = null;
				t.playerLoc = dest;
				//
				play(moveHackSound);
			}
		} else if (dest_contents[1] == 'V') {
			t.threatHit();
		} else { // Push a tile.
			t.pushTile(dest, move);
		}
		if (!t.player_hit) {
			// Move threats
			let player_hit_path = t.getPath(t.playerLoc, t.exfiltration);
			let new_threats = [];
			for (let i = t.threats.length-1; i >= 0; i--) {
				let ti = t.getLoc(t.threats[i]);
				let next = t.getPath(t.threats[i], t.playerLoc);
				console.log("n: " + next + " " + ti);
				if (next) {
					if (player_hit_path) { // If the player can be forced out, do so.
						if (mag(sub(next, t.playerLoc)) < 2) {
							t.threatHit();
						}
					}
					if (next[0] == t.playerLoc[0] && next[1] == t.playerLoc[1]) {
						next = t.threats[i];
					} else {
						t.getLoc(next)[1] = ti[1];
						ti[1] = null;
					}
					new_threats.push(next);
				} else {
					new_threats.push(t.threats[i]);
				}
			}
			t.threats = new_threats;
		}
	}

	render(ctx) {
		if (this.winTimer) {
			this.winTimer += 1;
			if (this.winTimer == 50) {
				this.winTimer = -1;
				objs.hackTower = false;
			}
		}
		// The display.
		ctx.fillStyle = '#11111155';
		// 
		ctx.beginPath();
		ctx.fillRect(0,0,screenWidth,screenHeight);
		ctx.fill();
		//
		ctx.strokeStyle = '#FFCCAA';
		ctx.font = '15px unicode';
		//
		let p = [screenWidth/4, screenHeight/4];
		ctx.fillStyle = '#11111190';
		ctx.beginPath();
		ctx.fillRect(p[0]-7,p[1]-15,screenWidth/2,screenHeight/2+2);
		ctx.fill();
		for (let x = 0; x < RL_SIZE[0]; x++) {
			for (let y = 0; y < RL_SIZE[1]; y++) {
				let xp = add(p, [screenWidth/2*x/RL_SIZE[0], screenHeight/2*y/RL_SIZE[1]]);
				let c = this.field[y][x];
				if (c[1] == null && IMPORTANT.indexOf(c[0]) == -1 &&
					((c[0] != '^') || (this.source.awake == true))) {
					ctx.strokeStyle = '#888888';
					ctx.strokeText(c[0], ...xp);
				} else {
					let obj = c[1] || c[0];
					ctx.strokeStyle = '#FFCCAA';
					ctx.shadowBlur = 5;
					ctx.strokeText(obj, ...xp);
					ctx.shadowBlur = 0;
				}
			}
		}
	}

}