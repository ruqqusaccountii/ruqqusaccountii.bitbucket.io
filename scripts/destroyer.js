const DSLIGHTS = [
	{x: 70, y: 5, w: 2, h: 1, b:5},
	{x: 73, y: 5, w: 2, h: 1, b:5},
	{x: 77, y: 5, w: 2, h: 1, b:5},

	{x: 69, y: 8, w: 15, h: 1, b:5},

	{x: 61, y: 8, w: 1, h: 6, b:5},
	{x: 88, y: 8, w: 1, h: 6, b:5},

	{x: 59, y: 27, w: 2, h: 1, b:5},
	{x: 95, y: 27, w: 2, h: 1, b:5},

	{x: 130, y: 29, w: 2, h: 2, b:5},
	{x: 122, y: 13, w: 2, h: 1, b:5},

	{x: 68, y: 55, w: 14, h: 1, b:5}, // upper body

	{x: 98, y: 56, w: 2, h: 1, b:5}, // Interior node
	{x: 99, y: 71, w: 1, h: 5, b:5},
	{x: 87, y: 71, w: 3, h: 1, b:5},
	{x: 93, y: 80, w: 2, h: 1, b:5},

	{x: 69, y: 131, w: 12, h: 1, b:5}, // mid-body
	{x: 92, y: 185, w: 1, h: 15, b:5},
	{x: 66, y: 76, w: 18, h: 1, b:5},

	{x: 98, y: 119, w: 9, h: 1, b:5}, // Secondary turret region
	{x: 120, y: 136, w: 1, h: 4, b:5},
	{x: 109, y: 145, w: 5, h: 2, b:5},
	{x: 94, y: 143, w: 5, h: 1, b:5},
	{x: 112, y: 151, w: 1, h: 4, b:5},
	{x: 96, y: 165, w: 5, h: 1, b:5},
	{x: 88, y: 145, w: 1, h: 1, b:5},
	{x: 102, y: 168, w: 2, h: 1, b:5},

	{x: 77, y: 240, w: 1, h: 1, b:5}, // lower body
	{x: 72, y: 268, w: 6, h: 1, b:5},
	{x: 74, y: 277, w: 2, h: 1, b:5},
	{x: 66, y: 295, w: 4, h: 1, b:5},
	{x: 80, y: 295, w: 4, h: 1, b:5},
	{x: 67, y: 303, w: 1, h: 1, b:5},
	{x: 82, y: 303, w: 1, h: 1, b:5},

	{x: 91, y: 234, w: 1, h: 23, b:5}, // Secondary engines
	{x: 93, y: 307, w: 8, h: 1, b:5},
	{x: 99, y: 294, w: 3, h: 1, b:5},
	{x: 86, y: 295, w: 3, h: 1, b:5},

	{x: 140, y: 245, w: 1, h: 2, b:5}, // Nodes 
	{x: 134, y: 260, w: 1, h: 2, b:5},
	{x: 138, y: 253, w: 1, h: 5, b:5},

	{x: 109, y: 48, w: 1, h: 11, b:5},
	{x: 106, y: 51, w: 1, h: 4, b:5},
	{x: 115, y: 47, w: 1, h: 3, b:5},
	{x: 116, y: 59, w: 1, h: 1, b:5},

	{x: 137, y: 80, w: 1, h: 6, b:5},
	{x: 137, y: 89, w: 1, h: 2, b:5},
	{x: 133, y: 97, w: 1, h: 6, b:5},
	{x: 119, y: 109, w: 5, h: 1, b:5},
	{x: 98, y: 89, w: 1, h: 2, b:5},

	{x: 87, y: 87, w: 1, h: 17, b:5},

	{x: 69, y: 108, w: 12, h: 1, b:5}, // body
	{x: 66, y: 112, w: 18, h: 1, b:5}, 

	{x: 14, y: 317, w: 11, h: 2, b:10}, // engines
	{x: 125, y: 317, w: 11, h: 2, b:10},
	{x: 32, y: 318, w: 14, h: 2, b:10},
	{x: 104, y: 318, w: 14, h: 2, b:10},
	{x: 67, y: 310, w: 16, h: 2, b:10}
];
let x = []
DSLIGHTS.forEach(light=>{
	if ((light['b'] == 5) && (light['x'] > 75) && (light['w'] < (light['x']-75))) {
		x.push({x: 150-light['x'], y:light['y'], w:light['w'], h:light['h'], b: 5});
	}
});
x.forEach(xlight=>{ DSLIGHTS.push(xlight); });
// Could have destroyer_inits around the destroyer as guards.
var DS_S = new Sprite('./images/destroyer.png', DSLIGHTS);
var DSMT_S = new Sprite('./images/destroyer_mainLight.png', []);
var DSST_S = new Sprite('./images/destroyer_secondaryLight.png', []);
var DSMMT_S = new Sprite('./images/destroyer_missileTurret.png', []);

/*const DS_INTERIOR = [
	"#########################",
	"#####...#.......#..*#####",
	"####....#...X...#....####",
	"####....##.....##....####",
	"#####.####.....####.#####",
	"##......#...*...#......##",
	"##......#.......#......##",
	"#........#######........#",
	"#.####......#......####.#",
	"##....##....#....##....##",
	"#.....#...........#.....#",
	"#.....###.#####.###.....#",
	"###.##.....#.#.....##.###",
	"#...####.##...##.####...#",
	"##.......#.....#.......##",
	"#...#.....#...#.....#...#",
	"#####.####.#.#.####.#####",
	"##..........#..........##",
	"####...#####.#####...####",
	"###..###.........###..###",
	"##......#..#.#..#......##",
	"#.......###...###.......#",
	"#......#...#.#...#......#",
	"##....##....#....##....##",
	"#########################"
];*/
const DS_INTERIOR = [
	"#########################",
	"#####...#.......#..*#####",
	"####.*:.#...X...#....####",
	"####....##.....##....####",
	"#####*####.....####*#####",
	"##......#...*...#......##",
	"##.*:...#.......#...:*.##",
	"#........#######........#",
	"#.####..*...#...*..####.#",
	"##....##....#....##....##",
	"#..*:.#.....*.....#.:*..#",
	"#.....###*#####*###.....#",
	"###*##.....#.#.....##*###",
	"#...####*##...##*####...#",
	"##..*.*:.#.....#.:*.*..##",
	"#...#.....#...#.....#...#",
	"#####*####.#.#.####*#####",
	"##..........#..........##",
	"####*..#####.#####..*####",
	"###..###...*.*...###..###",
	"##......#..#.#..#......##",
	"#..*:...###...###...:*..#",
	"#......#...#.#...#......#",
	"##.*:.##....#....##.:*.##",
	"#########################"
];
// 0 1 2
// 3   4
// 5 678
//  9 10
// 11 12 13 14 15
// 16 17
// 18 19 20 21 22
// 
const DS_TELEPORTERS = [0,4];
const DS_TELROOMS = [2,1]
const DS_TILES = [1,5,6,9,11,19,20,28,29,30,31];
const DS_TILROOMS = [0,3,4,5,8,12,14,18,22,18,22];
const DS_DOORS = [2,3,7,8,10,12,13,14,15,16,17,18,21,22,23,24,25,26,27];
const DS_DORROOMS = [[0,3],[2,4],[3,6],[4,7],[6,7],[6,9],[7,10],[5,11],[8,15],[9,12],[10,14],[11,12],[14,15],[12,16],[14,17],[16,18],[17,22],[19,20],[20,21]];
// Indices of teleporters in TK_OBJs
/*const TK_TELEPORTERS = [0, 1, 2, 4, 10, 12, 14, 16, 18, 20, 22, 24];
const TK_TELROOMS = [0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6, 6]
const TK_TILES = [3, 7, 8, 11, 15, 19, 23]; // Tiles directly correspond to rooms.
const TK_DOORS = [5, 6, 9, 13, 17, 21, 25];
const TK_DORROOMS = [[0,1], [1,2], [1,3], [3,4], [4,5], [5,6], [6,7]];*/
// At most seven numbers used.
// End state: In last room.
// Can always move between rooms in top corners.
/*
	012
	 3
	 4
	 5
	 6
	 7
*/
// Try a similar puzzle to the previous encounter, with more complexity.
function getDestroyerPuzzle(tel, telr, til, tilr, dor, dorr) {
	//
	let types = 9;
	let door_max = 3; 
	let telTypes = new Array(tel.length).fill(0);
	let dorTypes = new Array(dor.length).fill(0); // Door types. Prefaced with '-' if they start open.
	let tilTypes = new Array(til.length).fill(0);
	let doorStat = new Array(types).fill(0);
	let telix = 0; // The current teleporter group
	// At each step, go to a new room or toggle a switch that leads to an unexplored
	// state. If that state encapsulates 'In room 6 with door 6 open', end.
	// Upon concluding, randomize remaining door and teleporter types.
	let states = {}; // Maps a state string to its predecessor. 
	// [pred, tel, dor, til, doorStat, room, telix, startRoom]
	// doorStat is the toggle status of each door type, in sequence.
	// State string: 'telt, dort, tilt, doorStat, room'
	let stRooms = [5, 8]; // Room 20 isn't factored in, since there aren't any teleporters.
	let room = rChoice(stRooms); 
	let startRoom = room;
	let approach = [];
	while (room != 1) {
		//
		// If more than 100 states have been explored, restart. I think memory may be the issue.
		if (Object.keys(states).length > 200) {
			states = {};
			return getDestroyerPuzzle(tel, telr, til, tilr, dor, dorr);
		}
		//
		let options = []; // [type, index, value?]
		// If we're in the room we started in, we can switch to the other start room.
		// Go through a door.
		// Only set a certain number of doors, to discourage easy puzzles.
		dorCount = 0;
		for (let i = 0; i < dorTypes.length; i++) {
			if (dorTypes[i] < 0) { dorCount += 1; } // Only count doors that start open.
		}
		//
		for (let i = 0; i < dorr.length; i++) {
			let d = dorr[i];
			if ((d[0] == room) || (d[1] == room)) {
				let dType = dorTypes[i];
				let dest = d[1]==room?d[0]:d[1];
				if (dType == 0 && dorCount <= door_max) {
					for (let j = 0; j < types; j++) { // Pick a type for this door.
						let ja = j+1;
						if (doorStat[j] == 1) {
							options.push(['dornew', dest, i, ja]); // dest, index, type
						} else {
							options.push(['dornew', dest, i, -ja]);
						}
					}
				} else if ((doorStat[Math.abs(dType)-1] == 0 && dType < 0) ||
					(doorStat[Math.abs(dType)-1] == 1 && dType > 0)) {
					options.push(['dor', dest]);
				}
			}
		}
		// Use a teleporter
		for (let i = 0; i < telr.length; i++) {
			if (telr[i] == room) { // Is the teleporter in this room?
				// Is the teleporter's destination defined?
				if (telTypes[i] != 0) { // 
					for (let j = 0; j < telr.length; j++) {
						if (j != i && telTypes[j] == telTypes[i]) {
							options.push(['tel', j]);
							break;
						}
					}
				} else {
					// If not, pair it with another teleporter, randomly.
					for (let j = 0; j < telr.length; j++) {
						if (j != i && telTypes[j] == 0) {
							options.push(['telnew', i, j]);
						}
					}
				}
				
			}
		}
		// Toggle a switch. 
		for (let i = 0; i < tilr.length; i++) {
			if (tilr[i] == room) {
				if (tilTypes[i] == 0) { // New tile
					for (let j = 0; j < types; j++) {
						options.push(['tilnew', j+1, i]);
					}
				} else {
					options.push(['til', tilTypes[i]]);
				}
			}
		}
		// Switch to the other start room, if possible.
		for (let i = 0; i < stRooms.length; i++) {
			if (room==stRooms[i]) {
				for (let j = 0; j < stRooms.length; j++) {
					if (i != j) {
						options.push(['swr', stRooms[j]]);
					}
				}
			}
		}
		//
		// Pick an option. If the option leads to no unexplored states, remove it.
		optionPicked = false;
		let option = null;
		let stateDesc = [telTypes, room, telix, dorTypes, doorStat, tilTypes]; // Raw representation of current state
		let sv = null; // Raw representation of new state
		let backtrack = false;
		while (optionPicked == false) { // Pick an option that hasn't been explored.
			if (options.length == 0) { // If no new paths, backtrack.
				optionPicked = true;
				backtrack = true;
			} else {
				option = rChoice(options);
				let sstr = '';
				// Pick an action
				if (option[0] == 'tel') {
					let newRoom = telr[option[1]];
					sv = [telTypes, newRoom, telix, dorTypes, doorStat, tilTypes];
				} else if (option[0] == 'telnew') {
					let tempTelTypes = telTypes.slice();
					let newRoom = telr[option[2]];
					tempTelTypes[option[1]] = telix; // New teleporter pair
					tempTelTypes[option[2]] = telix;
					sv = [tempTelTypes, newRoom, telix+1, dorTypes, doorStat, tilTypes];
				} else if (option[0] == 'dor') {
					let newRoom = option[1];
					sv = [telTypes, newRoom, telix, dorTypes, doorStat, tilTypes];
				} else if (option[0] == 'dornew') {
					let newRoom = option[1];
					let tempDorTypes = dorTypes.slice();
					tempDorTypes[option[2]] = option[3];
					sv = [telTypes, newRoom, telix, tempDorTypes, doorStat, tilTypes];
				} else if (option[0] == 'til') { // type
					let tempDoorStat = doorStat.slice();
					tempDoorStat[option[1]-1] = (tempDoorStat[option[1]-1]+1)%2;
					sv = [telTypes, room, telix, dorTypes, tempDoorStat, tilTypes];
				} else if (option[0] == 'tilnew') { // type, tile index
					let tempTilTypes = tilTypes.slice();
					let tempDoorStat = doorStat.slice();
					tempTilTypes[option[2]] = option[1];
					tempDoorStat[option[1]-1] = (tempDoorStat[option[1]-1]+1)%2;
					sv = [telTypes, room, telix, dorTypes, tempDoorStat, tempTilTypes];
				} else if (option[0] == 'swr') {
					let newRoom = option[1];
					sv = [telTypes, newRoom, telix, dorTypes, doorStat, tilTypes];
				}
				// Handle the new state
				sstr = sv.join('@');
				if (states[sstr] == undefined) {
					optionPicked = true;
					states[sstr] = [stateDesc, sv]; // PrevState, state
					approach.push([option, sv, sstr]);
				} else {
					options.splice(options.indexOf(option), 1);
				}
			}
		}
		// 
		if (backtrack) {
			// Get the raw representation of the previous state.
			sv = states[stateDesc.join('@')][0];
			approach.splice(approach.length-1, 1);
		}
		// Enter new state
		telTypes = sv[0];
		room = sv[1];
		telix = sv[2];
		dorTypes = sv[3];
		doorStat = sv[4];
		tilTypes = sv[5];
		// Continue with the loop
	}
	//
	if (approach.length < 10) { // Try to avoid trivial puzzles
		states = {};
		return getDestroyerPuzzle(tel, telr, til, tilr, dor, dorr);
	}
	// Randomly initialize all other objects.
	// Randomly pair remaining teleporters
	let u = []; // Unset teleporters
	for (let i = 0; i < tel.length; i++) {
		if (telTypes[i] == 0) {
			u.push(i);
		}
	}
	while (u.length != 0) {
		let i = Math.floor(Math.random()*u.length);
		telTypes[u[i]] = telix;
		u.splice(i,1);
		i = Math.floor(Math.random()*u.length);
		telTypes[u[i]] = telix;
		u.splice(i,1);
		telix += 1;
	}
	// Randomly set all remaining door types.
	for (let i = 0; i < dor.length; i++) {
		if (dorTypes[i] == 0) {
			// Non-essential doors start out closed, to reduce trivial puzzles.
			dorTypes[i] = (Math.floor(Math.random()*types)+1);
		}
	}
	// Randomly set all remaining tiles..
	for (let i = 0; i < til.length; i++) {
		if (tilTypes[i] == 0) {
			tilTypes[i] = (Math.floor(Math.random()*types)+1);
		}
	}
	// Fill in the object array 
	let objs = new Array(tel.length+dor.length+til.length);
	//console.log(objs);
	// Teleporters
	for (let i = 0; i < tel.length; i++) {
		objs[tel[i]] = 'T'+telTypes[i];
	}
	// Tiles
	for (let i = 0; i < til.length; i++) {
		objs[til[i]] = tilTypes[i]+'*'+tilTypes[i];
		// Invert all associated door types
		for (let j = 0; j < dor.length; j++) {
			if (Math.abs(dorTypes[j]) == tilTypes[i]) {
				dorTypes[j] *= -1;
			}
		}
	}
	// Doors
	for (let i = 0; i < dor.length; i++) {
		objs[dor[i]] = dorTypes[i]>0?'+'+dorTypes[i]:''+dorTypes[i];
	}
	console.log(objs);
	console.log(approach);
	return objs; // The object array for this puzzle.
}

// Missiles. Treated as an attribute of the main encounter.
// Track the player for a set period of time, teleporting him to a random point around the
// destroyer
class Missile {
	constructor(loc, dir, source, target) {
		this.angle = dir;
		this.l = loc.slice();
		this.target = target;
		this.source = source;
		this.acc = .3;
		this.max_vel = 5;
		this.turnRate = .03;
		this.v = pt(this.acc, this.angle);
		this.time = 0;
	}

	render(ctx) {

	}

	update(objs) {
		this.time += 1;
		let diff = sub(this.target.l, this.l)
		this.angle = ang_turn(this.angle, ang(diff), this.turnRate);
		this.v = add(this.v, pt(this.acc, this.angle));
		this.l = add(this.l, this.v)
		if (mag(this.v) > this.max_vel) {
			this.v = pt(this.max_vel, ang(this.v));
		}

		let c = 1.5;
		let pl = this.l.slice();
		let pv = perturb(add(mul(this.v,1), pt(-c, this.angle)), .1);
		objs.particles.push(new Particle(pl, pv, 3));

		// Upon impacting target, teleport the target.
		if (mag(diff) < 10) {
			let s = this.target.l.slice();
			let d = add(this.source.l, pt(700 + Math.random() * 300, 2*Math.random()*Math.PI));
			let diff = sub(d, s);
			let dist = mag(diff);
			let diff_ang = ang(diff);
			while (dist > 0) {
				dist -= 30;
				let loc = add(s, pt(dist, diff_ang));
				objs.particles.push(new Particle(loc, perturb([0,0], .1), 5));
			}
			this.target.l = d;
			if (this.target == objs.player) {
				play(teleportSound);
				addUniqueMessage('You feel the same leaping sensation as in the fortress, though there is a sense of a much deeper pit below you.');
			}
			this.time = Infinity;
		}
	}
}

// Occasionally change direction
class Destroyer {
	constructor(loc) {
		this.s = DS_S;
		this.l = loc.slice();

		this.v = [0,0];
		this.max_vel = 1;
		this.acc = .25;
		this.turnRate = .0025;
		this.thrusting = false;
		// Point within 70 degrees of the location's angle.
		this.angle = this.startingAngle = this.targetAngle = (.5-Math.random())*140/180*Math.PI + ang(loc);
		this.angleTimer = 1000+Math.random() * 1000;
		this.head_angle = this.angle;
		this.awake = true;

		this.entryMessage = "The Destroyer seems almost alive, coursing with intelligence. The walls seem to whisper to you; test you.";
		this.eps = [];
		this.eps.push(new EntryPoint([31-75, 75], [9,21], this, 9));
		this.eps.push(new EntryPoint([119-75, 75], [9,3], this, 9));
		this.eps.push(new EntryPoint([75-75, -50], [21,12], this, 9));
		console.log('Generating destroyer puzzle.');
		let iobjs = getDestroyerPuzzle(DS_TELEPORTERS, DS_TELROOMS, DS_TILES, DS_TILROOMS, DS_DOORS, DS_DORROOMS);
		console.log('Done.');
		//let iobjs = ['T1', 'T1']
		this.interior = new Roguelike(this, DS_INTERIOR, iobjs);
		//
		this.setLs();
		this.initTurrets();
		//
		this.towerRotateSound = document.createElement("audio");
		this.towerRotateSound.src = './audio/towerrotate.mp3';
	}

	initTurrets() {
		this.turrets = []
		// (loc, base_angle, arc, turnRate, sprite, lightOffset, range, ship)
		// Add the primary turret
		this.turrets.push(
			new Turret([75-75, 160-35], 0, 2.8, .005, DSMT_S, 15, 300, this)
		);
		// Add the secondary turrets
		this.turrets.push(
			new Turret([75-45, 160-133], Math.PI * 5/8, 2.1, .007, DSST_S, 8, 200, this)
		);
		this.turrets.push(
			new Turret([75-106, 160-133], Math.PI * 11/8, 2.1, .007, DSST_S, 8, 200, this)
		);
		// Add a missile turret
		this.missileTurret = new Turret(
			[75-75,160-94], 0, -1, .01, DSMMT_S, 10, 50, this);
		this.turrets.push(this.missileTurret);
		this.missileTurret.rate = 200;
		this.missileTurret.charge = 0;
		this.missiles = [];
		
	}

	hackStart() {
		objs.player.pauseSounds();
		pause(this.towerRotateSound);
	}

	render(ctx) {
		let t = this;
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let l = add(t.l, offset);
		t.s.draw(ctx, l[0], l[1], t.angle);
		//
		t.eps.forEach(e=>{
			e.render(ctx, offset);
		});
		//
		this.turrets.forEach(x=>{
			x.render(ctx);
		});
		//
		this.missiles.forEach(x=>{
			x.render(ctx);
		});
	}

	setLs() {
		this.hl = add(pt(-10, this.angle), this.l);
		this.hps = [];
	}

	update(objs) {
		let t = this;
		//
		let mv = mag(t.v);
		if (mv > t.max_vel) {
			t.v = pt(t.max_vel, ang(t.v));
		} else if (!t.thrusting && mv > t.max_vel/2){
			t.v = mul(t.v, .995);
		}
		t.l = add(t.l, t.v);
		//
		let turret_turning = false;
		let shooting = false;
		t.setLs();
		//
		let aiming = false;
		//
		t.eps.forEach(e=>{
			e.update(objs);
		});
		for (let i = t.missiles.length-1; i >= 0; i--) {
			t.missiles[i].update(objs);
			if (t.missiles[i].time > 500) {
				t.missiles.splice(i, 1);
			}
		}
		if (t.awake) {
			// Handle turning
			let turnMod = 0;
			this.angleTimer = this.angleTimer - 1;
			if (this.angleTimer <= 0) {
				this.angleTimer = 1000+Math.random() * 1000;
				this.targetAngle = ang_norm(this.startingAngle + (.5-Math.random()) * Math.PI);
			}
			if (this.targetAngle != -1) {
				t.angle = ang_turn(t.angle, t.targetAngle, t.turnRate);
				turnMod = 2*ang_dir(t.targetAngle, t.angle);
				if (t.angle == t.targetAngle) { t.targetAngle = -1; }
			}
			// Thrust forwards while awake
			[[14+6, 317+29,11-turnMod],[125+6, 317+29,11+turnMod],[67+6, 310+29,16],
				[32+7,318+29,14],[104+7,318+29,14]].forEach(m=>{
				let c = 1.5;
				let pl = add(t.l, pt_addAngle([m[0]-80+m[2]/2, 190-m[1]], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(-c, t.angle)), .1);
				objs.particles.push(new Particle(pl, pv, m[2]/3));
			});
			t.v = add(t.v, [t.acc*Math.sin(t.angle), t.acc*Math.cos(t.angle)]);
			t.thrusting = true;	
			// Missile turret
			this.missileTurret.charge += 1;
			if (this.missileTurret.charge > this.missileTurret.rate) {
				let diff = sub(objs.player.l, this.l);
				let dist = mag(diff);
				if (dist < 1000 && ang_dist(ang(diff), this.missileTurret.angle) < 1.5 ) {
					this.missiles.push(new Missile(this.missileTurret.l, this.missileTurret.angle, this, objs.player));
					this.missileTurret.charge = 0;
				}
			}		
		} else {
			t.v = mul(t.v, .995);
		}
		//
		let target = null;
		if (this.awake && mag(sub(t.l, objs.player.l)) < 600) {
			target = objs.player;
		}
		//
		t.hps = [];
		t.turrets.forEach(x=>{
			turret_turning = x.update(objs, target) || turret_turning;
			t.hps.push([x.l, x.sprite.radius]);
		});
		//
		if (!objs.hackTower) {
	 		if (turret_turning) {
				play(this.towerRotateSound);
			} else {
				pause(this.towerRotateSound);
			}
		}
	}
}