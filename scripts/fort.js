var FORT_LIGHTS = [
	{x: 212, y: 24, w: 2, h: 5, b:5},
	{x: 67, y: 24, w: 2, h: 5, b:5},
	{x: 137, y: 6, w: 9, h: 1, b:5},
	{x: 180, y: 6, w: 1, h: 5, b:5},
	{x: 101, y: 10, w: 1, h: 5, b:5},
	{x: 228, y: 62, w: 2, h: 2, b:5},
	{x: 136, y: 38, w: 3, h: 1, b:5},
	{x: 142, y: 38, w: 4, h: 1, b:5},
	{x: 141, y: 49, w: 1, h: 30, b:5},

	{x: 228, y: 62, w: 2, h: 2, b:5},
	{x: 229, y: 248, w: 2, h: 2, b:5},
	{x: 263, y: 122, w: 7, h: 1, b:5},
	{x: 231, y: 154, w: 1, h: 10, b:5},
	{x: 248, y: 176, w: 6, h: 1, b:5},

	{x: 56, y: 62, w: 2, h: 2, b:5},
	{x: 23, y: 129, w: 9, h: 1, b:5},
	{x: 55, y: 148, w: 1, h: 17, b:5},
	{x: 17, y: 189, w: 7, h: 1, b:5},
	{x: 57, y: 248, w: 2, h: 2, b:5},

	{x: 144, y: 164, w: 5, h: 1, b:5},
	{x: 143, y: 174, w: 7, h: 1, b:5},
	{x: 142, y: 183, w: 9, h: 1, b:5},
	{x: 144, y: 204, w: 5, h: 1, b:5},
	{x: 127, y: 175, w: 1, h: 1, b:5},
	{x: 166, y: 175, w: 1, h: 1, b:5},

	{x: 121, y: 209, w: 2, h: 2, b:5},
	{x: 114, y: 231, w: 2, h: 2, b:5},
	{x: 132, y: 249, w: 2, h: 2, b:5},
	{x: 159, y: 248, w: 2, h: 2, b:5},
	{x: 174, y: 232, w: 2, h: 2, b:5},
	{x: 168, y: 208, w: 2, h: 2, b:5}
];
var FORT_SPRITE = new Sprite('./images/fort.png', FORT_LIGHTS);

function getFortPuzzle(rooms, s, e) {
	// For each room, calculate degree.
	let rd = {};
	let pairings = [];
	rooms.forEach(r=> {
		if (!rd[r]) {
			rd[r] = 0;
		}
		rd[r] += 1;
	});
	// Pick an arbitrary starting room for the guaranteed path.
	let st = s[Math.floor(Math.random()*s.length)] + '';
	// While we have not reached the end room, pair a teleporter from the current room
	// with a teleporter from a random room that is either the goal or has degree >= 2
	let vr = st;
	let tid = 0;
	while (st != e) {
		let vr = [];
		for (let room in rd) {
			if (room != st && (room == e || rd[room] >= 2)) {
				vr.push(room);
			}
		}
		// Pair st with a random room
		vr = vr[Math.floor(Math.random()*vr.length)];
		rd[st] -= 1;
		rd[vr] -= 1;
		pairings.push([st, vr]);
		st = vr;
	}
	// Define and populate the output array
	let res = new Array(rooms.length);
	for (let i = 0; i < pairings.length; i++) {
		let p = pairings[i];
		// Get the index of the first teleporter
		for (let i2 = 0; i2 < rooms.length; i2++) {
			if (''+rooms[i2] == p[0] && !res[i2]) {
				res[i2] = 'T'+tid;
				break;
			}
		}
		// Get the index of the second teleporter
		for (i2 = 0; i2 < rooms.length; i2++) {
			if (''+rooms[i2] == p[1] && !res[i2]) {
				res[i2] = 'T'+tid;
				break;
			}
		}
		tid += 1;
	}
	// Randomly pair teleporters until the total degree remaining is zero.
	vr = [];
	for (i = 0; i < res.length; i++) {
		if (!res[i]) {
			vr.push(i);
		}
	}
	while (vr.length > 0) {
		let ix = Math.floor(Math.random()*vr.length);
		res[vr[ix]] = 'T' + tid;
		vr.splice(ix, 1);
		ix = Math.floor(Math.random()*vr.length);
		res[vr[ix]] = 'T' + tid;
		vr.splice(ix, 1);
		tid += 1;
	}
	return res;
}

const FORT_INTERIOR = [
	"...###################...",
	"..##..#..#.....#..#.*###.",
	".#..........*.#........#.",
	".######.##...#....######.",
	".......#..#...#.*#.......",
	"..##....#.#..#..#...###..",
	".#.#....#*#######...#.*#.",
	".#.*#...###....#...#...#.",
	"#*...#..#**#..##..#*....#",
	"##....#.#*#*##.*#.##...##",
	"#.###*#..##*##*.#.#.###*#",
	"#....#.##..#..##.#......#",
	"#.......##.###..#.*..####",
	"#.*...##..#...##.#..#...#",
	"#.##.#...#.*.#*.#.##....#",
	"##*.##....#*.#.*#.#.####.",
	"#.**#......###*#..#.....#",
	"#..#..........#....#....#",
	".##........###......#..#.",
	".......#...#*#...#..###..",
	"############.############",
	"#...*..#...#.#.#.#..*...#",
	"##..*..#....X..#....*..##",
	"##.#.#.#.#.#.#.#.#.#.#.##",
	"#########################"
];
const FORT_ROOMS = [2, 1, 2, 3, 4, 5, 5, 6, 6, 4, 6, 7, 8, 5, 7, 8, 10, 10, 9, 11, 12, 13, 11, 12, 13, 13, 12, 14, 15, 16, 15, 16];

class Fort {
	constructor(loc) {
		this.bs = FORT_SPRITE;
		this.l = loc.slice();
		this.angle = Math.random()*2*Math.PI;
		this.head_angle = ang_norm(this.angle+Math.PI);
		this.setLs();
		this.awake = true;
		//
		this.initInterior();
		this.entryMessage = "The ancient structure is covered partially in heavy plating. It was clearly built to last, but time has taken its toll.";
		this.eps = [];
		this.eps.push(new EntryPoint([0,-110], [21,11], this, 12));
		this.eps.push(new EntryPoint([110,-39], [12, 3], this, 12));
		this.eps.push(new EntryPoint([-110,-39], [11,23], this, 12));
		this.eps.push(new EntryPoint([0,95], [5, 12], this, 12));
		//
		this.light = new Light(this, 19, 750);
	}

	initInterior() {
		let objs = getFortPuzzle(FORT_ROOMS, [1, 9, 10], 14);
		this.interior = new Roguelike(this, FORT_INTERIOR, objs);
	}

	hackStart(objs) {
		objs.player.pauseSounds();
 	}

	render(ctx) {
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let bl = add(this.tl, offset);
		let hl = add(this.hl, offset);
		this.bs.draw(ctx, bl[0], bl[1], this.angle);
		//
 		this.eps.forEach(e=>{
 			e.render(ctx, offset);
 		});
 		if (this.awake) {
 			this.light.render(ctx, offset);
 		}
	}

	setLs() {
		this.hl = add(pt(-70, this.angle), this.l);
		this.tl = add(pt(-15, this.angle), this.l);
		this.hps = [[this.hl, 36]];
	}

	update(objs) {
		let t = this;
		let turning = false;
		let shooting = false;
		t.setLs();
		//
		let aiming = false;
		if (this.awake) {
			let p = objs.player;
			let diff = sub(p.l, t.hl);
			let dist = mag(diff);
			this.eps.forEach(x=>{
				x.update(objs);
			});
			this.light.update(objs);
		}
	}
}