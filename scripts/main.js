function play(sound) {
	try {
		if (SOUND) {
			sound.play()
		}
	} catch (e) {}
}
function pause(sound) {
	if (SOUND) {
		sound.pause()
	}
}

class Particle {
	constructor(l, v, radius) {
		this.l = l.slice()
		this.v = v.slice()
		this.radius = radius;
	}
	update(list) {
		this.radius = this.radius * .90;
		this.l = add(this.l, this.v);
		if (this.radius < .1) {
			list.splice(list.indexOf(this), 1);
		}
	}
	render(ctx) {
		ctx.fillStyle = ctx.shadowColor = '#FFCCAA';
		ctx.shadowBlur = 10;
		ctx.beginPath();
		let vl = add(sub(this.l, player.l), [screenWidth/2, screenHeight/2]);
		ctx.arc(vl[0], vl[1], this.radius, 0, 2*Math.PI);
		ctx.fill();
		ctx.shadowBlur = 0;
	}
}

var sound = document.createElement("audio");
sound.src = './audio/shot.mp3';
var engineSound = document.createElement("audio");
engineSound.src = './audio/engine.mp3';

var beginHackSound = document.createElement("audio");
beginHackSound.src = './audio/beginhack.mp3';

var moveHackSound = document.createElement("audio");
moveHackSound.src = './audio/movehack.mp3';

var pushHackSound = document.createElement("audio");
pushHackSound.src = './audio/pushhack.mp3';

var teleportSound = document.createElement("audio");
teleportSound.src = './audio/teleport.mp3';