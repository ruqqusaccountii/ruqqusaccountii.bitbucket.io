 var PSLIGHTS_TOP = [
	 	{x: 26, y: 1, w: 19, h: 2, b:30},
	 	{x: 35, y: 21, w: 1, h: 4, b:5},
	 	{x: 35, y: 27, w: 1, h: 4, b:5},
	 	{x: 31, y: 67, w: 9, h: 2, b:9},

	 	{x: 19, y: 29, w: 9, h: 3, b:5},
	 	{x: 14, y: 61, w: 2, h: 1, b:9},

	 	{x: 71-19-9, y: 29, w: 9, h: 3, b:5},
	 	{x: 71-14-2, y: 61, w: 2, h: 1, b:9}
	 ];
	 var PSLIGHTS_BOTTOM = [
	 	{x: 74, y: 5, w: 4, h: 1, b:5},
	 	{x: 60, y: 7, w: 9, h: 1, b:5},
	 	{x: 152-60-9, y: 7, w: 9, h: 1, b:5},

	 	{x: 28, y: 119, w: 1, h: 2, b:5},
	 	{x: 29, y: 170, w: 1, h: 1, b:5},
	 	{x: 40, y: 219, w: 2, h: 8, b:5},
	 	{x: 12, y: 193, w: 1, h: 60, b:5},

	 	{x: 152-28-2, y: 119, w: 1, h: 2, b:5},
	 	{x: 152-29-1, y: 170, w: 1, h: 1, b:5},
	 	{x: 152-40-8, y: 219, w: 2, h: 8, b:5},
	 	{x: 152-12-1, y: 193, w: 1, h: 60, b:5},

	 	{x: 30, y: 278, w: 22, h: 2, b:10},
	 	{x: 152-30-22, y: 278, w: 22, h: 2, b:10},
	 ];
	 var PSLIGHTS_DRONE = [
	 	{x: 26, y: 7, w: 2, h: 1, b:5},
	 	{x: 63-26-2, y: 7, w: 2, h: 1, b:5},

	 	{x: 28, y: 14, w: 2, h: 1, b:5},
	 	{x: 63-28-2, y: 14, w: 2, h: 1, b:5},

	 	{x: 17, y: 24, w: 1, h: 3, b:5},
	 	{x: 63-17-2, y: 24, w: 1, h: 3, b:5},

	 	{x: 5, y: 29, w: 1, h: 2, b:5},
	 	{x: 63-5-2, y: 29, w: 1, h: 2, b:5},

	 	{x: 19, y: 41, w: 4, h: 2, b:10}, // Engines
	 	{x: 63-19-4, y: 41, w: 4, h: 2, b:10},
	 	{x: 28, y: 41, w: 7, h: 2, b:10},
	 ];
	 var PS_S = new Sprite('./images/patrolship.png', PSLIGHTS_BOTTOM);
	 var PST_S = new Sprite('./images/psturret.png', PSLIGHTS_TOP);
	 var PSD_S = new Sprite('./images/psdrone.png', PSLIGHTS_DRONE);

	const PS_INTERIOR = [
	 	"#########################",
	 	"########.........########",
	 	"#.......................#",
	 	"#.......................#",
	 	"#..........:2*..........#",
	 	"#.......................#",
	 	"#.......................#",
	 	"########.........########",
	 	"########.........########",
	 	"############*############",
	 	"#######.....X.....#######",
	 	"######.............######",
	 	"#######.....*.....#######",
	 	"######.............######",
	 	"#######...........#######",
	 	"############*############",
	 	"########.........########",
	 	"########.........########",
	 	"#.......................#",
	 	"#.......................#",
	 	"#..........:1*..........#",
	 	"#.......................#",
	 	"#.......................#",
	 	"########.........########",
	 	"#########################"
	 ];
	 const PS_OBJS = ['2','+2','V','+1', '1'];

	 class Drone {
		constructor(source) {
			this.sprite = PSD_S;
			this.l = this.hl = source.l.slice();
			this.v = [0,0]
			this.angle = this.head_angle = 0;
			this.max_vel = 2;
			this.acc = .15;
			this.turn_rate = .025;
			//
			this.source = source;
			this.lr = this.base_lr = 300;
			this.light = new Light(this, 30, this.lr);
			this.eps = source.eps;
		}

		render(ctx) {
			let offset = sub([screenWidth/2, screenHeight/2], player.l)
			let dl = add(this.l, offset);
			this.sprite.draw(ctx, dl[0], dl[1], this.angle);
			if (this.source.awake) {
				this.light.render(ctx, offset);
			}
		}

		update(objs) {
			let t = this;
			//
			let mv = mag(t.v);
			if (mv > t.max_vel) {
				t.v = pt(t.max_vel, ang(t.v));
			} else if (!t.thrusting && mv > t.max_vel/2){
				t.v = mul(t.v, .995);
			}
			t.l = add(t.l, t.v);
			//
			if (t.source.awake) {
				let target_angle = t.angle;
				let strafe_right = false;
				let strafe_left = false;
				let thrust = false;
				let reverse = false;
				//
				// If we're too far from the source, turn towards the source and thrust.
				let s_diff = sub(t.source.l, t.l);
				let s_dist = mag(s_diff);
				if (s_dist > 300) {
					target_angle = ang(s_diff);
					thrust = true;
				} else if (s_dist < 100) { // If we're too close to the source, turn away from the source and thrust.
					target_angle = ang_norm(ang(s_diff)+Math.PI);
					thrust = true;
				} else {
					let p = objs.player;
					let p_diff = sub(p.l, t.l);
					let p_s_diff = sub(t.source.l, p.l);
					if (mag(p_s_diff) > 300) { // If the player is too far away, orbit the source.
						target_angle = ang_norm(ang(s_diff)+Math.PI/2);
						thrust = true;
					} else {  // Otherwise, if the player is within range, face the player
						target_angle = ang_norm(ang(p_diff));
					}
				}
				
				//
				let temp = t.angle;
				t.angle = ang_turn(t.angle, target_angle, t.turn_rate);
				if (t.angle != temp) {
					let c = 2;
					let dir = -1*ang_dir(temp, t.angle);
					let pl = add(t.l, pt_addAngle([dir*5, 21], t.angle));
					let pv = perturb(add(mul(t.v,1), pt(c, t.angle+dir*Math.PI/2)), .5);
					objs.particles.push(new Particle(pl, pv, 2));
				}
				//
				if (thrust) {
					//
					[-1,0,1].forEach(m=>{
						//
						let x = Math.abs(m);
						let c = 2.5;
						let pl = add(t.l, pt_addAngle([m*7, -23+x], t.angle));
						let pv = perturb(add(mul(t.v,1), pt(-c, t.angle)), .1);
						objs.particles.push(new Particle(pl, pv, 5-x*2));
					});
					//
					t.v = add(t.v, [t.acc*Math.sin(t.angle), t.acc*Math.cos(t.angle)]);
				}
				if (reverse) {
					[-1,1].forEach(m=>{
						//
						let c = 2;
						let pl = add(t.l, pt_addAngle([m*20, 5], t.angle));
						let pv = perturb(add(mul(t.v,1), pt(c, t.angle)), .3);
						objs.particles.push(new Particle(pl, pv, 3));
					});
					t.v = add(t.v, [t.acc/4*-Math.sin(t.angle), t.acc/4*-Math.cos(t.angle)]);
				}
				//
				if (strafe_right) {
					let c = 2.5;
					let dir = -1;
					let a = t.angle+dir*Math.PI/2;
					let pl = add(t.l, pt_addAngle([dir*-15, -17], t.angle));
					let pv = perturb(add(mul(t.v,1), pt(-c, a)), .3);
					objs.particles.push(new Particle(pl, pv, 3));
					//
					t.v = add(t.v, [t.acc/2*Math.sin(a), t.acc/2*Math.cos(a)]);
				}
				if (strafe_left) {
					let c = 2.5;
					let dir = 1;
					let a = t.angle+dir*Math.PI/2;
					let pl = add(t.l, pt_addAngle([dir*-15, -17], t.angle));
					let pv = perturb(add(mul(t.v,1), pt(-c, a)), .3);
					objs.particles.push(new Particle(pl, pv, 3));
					//
					t.v = add(t.v, [t.acc/2*Math.sin(a), t.acc/2*Math.cos(a)]);
				}
			} else { // Slow to a halt once offline.
				t.v = mul(t.v, .995);
			}
			//
			t.head_angle = t.angle;
			t.hl = t.l;
			if (t.source.awake) {
				t.light.update(objs);
			}
		}
	}

	 class PatrolShip {
	 	constructor(loc) {
	 		this.hs = PST_S;
	 		this.bs = PS_S;
	 		this.l = loc.slice();

	 		this.v = [0,0];
	 		this.max_vel = .5;
	 		this.acc = .15;
	 		this.thrusting = false;

	 		// Point within 70 degrees of the location angle
	 		this.angle = (.5 - Math.random())*Math.PI*140/180 + ang(loc);
	 		this.head_angle = this.angle;
	 		this.awake = true;

	 		this.entryMessage = "You hear a faint, heavy breathing from the ship's control room.";
	 		this.eps = [];
	 		this.eps.push(new EntryPoint([0, 60], [2,12], this));
	 		this.eps.push(new EntryPoint([0, -80], [22,12], this));
	 		this.interior = new Roguelike(this, PS_INTERIOR, PS_OBJS);
	 		//
	 		this.drone = new Drone(this);
	 		//
	 		this.setLs();
	 		//
	 		this.light = new Light(this, 50, 600);

	 		this.towerRotateSound = document.createElement("audio");
			this.towerRotateSound.src = './audio/towerrotate.mp3';
	 	}

	 	hackStart() {
	 		objs.player.pauseSounds();
	 		pause(this.towerRotateSound);
	 	}

	 	render(ctx) {
	 		let t = this;
	 		let offset = sub([screenWidth/2, screenHeight/2], player.l);
	 		let bl = add(t.l, offset);
	 		let hl = add(t.hl, offset);
	 		t.bs.draw(ctx, bl[0], bl[1], t.angle);
	 		//
	 		t.eps.forEach(e=>{
	 			e.render(ctx, offset);
	 		});
	 		//
	 		t.hs.draw(ctx, hl[0], hl[1], t.head_angle);
	 		if (t.awake) {
	 			t.light.render(ctx, offset);
	 		}
	 		t.drone.render(ctx, offset);
	 	}

	 	setLs() {
	 		this.hl = add(pt(-10, this.angle), this.l);
	 		this.hps = [[this.hl, PST_S.radius], [this.drone.l, PSD_S.radius]];
	 	}

	 	update(objs) {
	 		let t = this;
	 		//
	 		let mv = mag(t.v);
	 		if (mv > t.max_vel) {
	 			t.v = pt(t.max_vel, ang(t.v));
	 		} else if (!t.thrusting && mv > t.max_vel/2){
	 			t.v = mul(t.v, .995);
	 		}
	 		t.l = add(t.l, t.v);
	 		//
	 		let turning = false;
	 		let shooting = false;
	 		t.setLs();
	 		//
	 		let aiming = false;
			//
	 		t.eps.forEach(e=>{
	 			e.update(objs);
	 		});
	 		this.drone.update(objs);
	 		// Thrust forwards while awake
	 		if (t.awake) {
	 			// Handle lights
	 			this.light.update(objs);
	 			//
	 			let p = objs.player;
	 			let diff = sub(p.l, t.hl);
	 			let dist = mag(diff);
	 			//
	 			//this.light.range = Math.min(dist, 900);
	 			
	 			//
	 			if (this.light.maxRange > dist && ang_dist(t.head_angle, ang(diff)) > .005) {
	 				aiming = true;
	 				t.head_angle = ang_turn(t.head_angle, ang(diff), .005);
	 				turning = true;
	 			}
 				//
				[-1,1].forEach(m=>{
					//
					let c = 1.5;
					let pl = add(t.l, pt_addAngle([m*35, -139], t.angle));
					let pv = perturb(add(mul(t.v,1), pt(-c, t.angle)), .1);
					objs.particles.push(new Particle(pl, pv, 7));
					//
					t.v = add(t.v, [t.acc*Math.sin(t.angle), t.acc*Math.cos(t.angle)]);
					t.thrusting = true;
				});
			} else { // Slow to a halt once offline.
				t.v = mul(t.v, .995);
			}
	 		//
	 		if (!aiming && t.head_angle != t.angle) {
	 			t.head_angle = ang_turn(t.head_angle, t.angle, .01);
	 			turning = true;
	 		}

	 		if (!objs.hackTower) {
		 		if (turning) {
					play(this.towerRotateSound);
				} else {
					pause(this.towerRotateSound);
				}
			}
	 	}
	 }