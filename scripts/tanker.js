const TKLIGHTS = [
	{x: 73, y: 10, w: 5, h: 1, b:5}, // Central cabin
	{x: 81, y: 10, w: 9, h: 1, b:5},
	{x: 79, y: 14, w: 1, h: 3, b:5},
	{x: 79, y: 17, w: 1, h: 2, b:5},
	{x: 79, y: 19, w: 1, h: 2, b:5},
	{x: 81, y: 21, w: 1, h: 2, b:5},

	{x: 14, y: 22, w: 1, h: 24, b:5}, // Left cabin
	{x: 66, y: 17, w: 1, h: 12, b:5},
	{x: 48, y: 82, w: 7, h: 1, b:5},
	{x: 11, y: 64, w: 1, h: 7, b:5},
	{x: 47, y: 74, w: 8, h: 1, b:5},

	{x: 103, y: 72, w: 1, h: 5, b:5}, // Right cabin
	{x: 108, y: 82, w: 7, h: 1, b:5},
	{x: 126, y: 70, w: 1, h: 6, b:5},
	{x: 151, y: 64, w: 1, h: 7, b:5},
	{x: 148, y: 22, w: 1, h: 24, b:5},
	{x: 115, y: 8, w: 11, h: 1, b:5},
	{x: 96, y: 17, w: 1, h: 12, b:5},

	{x: 36, y: 117, w: 1, h: 24, b:5},
	{x: 125, y: 117, w: 1, h: 24, b:5},
	{x: 78, y: 167, w: 4, h: 1, b:5},

	{x: 26, y: 185, w: 2, h: 2, b:5},
	{x: 31, y: 191, w: 2, h: 1, b:5},
	{x: 132, y: 185, w: 2, h: 2, b:5},
	{x: 130, y: 191, w: 2, h: 1, b:5},

	{x: 58, y: 192, w: 1, h: 12, b:5},
	{x: 103, y: 192, w: 1, h: 12, b:5},

	{x: 71, y: 229, w: 1, h: 2, b:5},
	{x: 91, y: 229, w: 1, h: 2, b:5},

	// Engines
	{x: 18, y: 373, w: 19, h: 2, b:10},
	{x: 43, y: 373, w: 14, h: 2, b:10},
	{x: 68, y: 377, w: 27, h: 2, b:10},
	{x: 103, y: 373, w: 14, h: 2, b:10},
	{x: 124, y: 373, w: 19, h: 2, b:10},
];
var TK_S = new Sprite('./images/tanker.png', TKLIGHTS);
var TKMT_S = new Sprite('./images/tankerTurret.png', []);
var TKST_S = new Sprite('./images/tankerTurretSecondary.png', []);

const TK_INTERIOR = [
	"#########################",
	"###...##*.......*##...###",
	"##*...##....*....##...*##",
	"##.....*....:....*.....##",
	"##.*:.###.......###.*:.##",
	"###...#...........#...###",
	"#######.####*####.#######",
	"########.........########",
	"#######*.....*:..*#######",
	"######.##.......##.######",
	"#######..###*###..#######",
	"#######*.....*:..*#######",
	"######.#.........#.######",
	"######..####*####..######",
	"######.*.....*:..*.######",
	"########.........########",
	"######..####*####..######",
	"######.*.....*:..*.######",
	"########.........########",
	"#####..#.........#..#####",
	"#####..#####*#####..#####",
	"#######...........#######",
	"####...#....X....#...####",
	"####...#.........#...####",
	"#########################"
];
const TK_OBJS = [];
// Indices of teleporters in TK_OBJs
const TK_TELEPORTERS = [0, 1, 2, 4, 10, 12, 14, 16, 18, 20, 22, 24];
const TK_TELROOMS = [0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6, 6]
const TK_TILES = [3, 7, 8, 11, 15, 19, 23]; // Tiles directly correspond to rooms.
const TK_DOORS = [5, 6, 9, 13, 17, 21, 25];
const TK_DORROOMS = [[0,1], [1,2], [1,3], [3,4], [4,5], [5,6], [6,7]];
// At most seven numbers used.
// End state: In last room.
// Can always move between rooms in top corners.
/*
	012
	 3
	 4
	 5
	 6
	 7
*/
// Start with a teleporter puzzle using the new algorithm design.
// State is just room and teleporter types.
// Randomize tiles and doors once done generating.
function getTankerPuzzle(tel, telr, til, dor, dorr) {
	//
	let telTypes = new Array(tel.length).fill(0);
	let dorTypes = new Array(dor.length).fill(0); // Door types. Prefaced with '-' if they start open.
	let tilTypes = new Array(til.length).fill(0);
	let doorStat = new Array(7).fill(0);
	let telix = 0; // The current teleporter group
	// At each step, go to a new room or toggle a switch that leads to an unexplored
	// state. If that state encapsulates 'In room 6 with door 6 open', end.
	// Upon concluding, randomize remaining door and teleporter types.
	let states = {}; // Maps a state string to its predecessor. 
	// [pred, tel, dor, til, doorStat, room, telix, startRoom]
	// doorStat is the toggle status of each door type, in sequence.
	// State string: 'telt, dort, tilt, doorStat, room'
	let room = rChoice([0, 2]);
	let startRoom = room;
	while (room != 7) {
		// If more than 100 states have been explored, restart. I think memory may be the issue.
		if (Object.keys(states).length > 100) {
			states = {};
			return getTankerPuzzle(tel, telr, til, dor, dorr);
		}
		let options = []; // [type, index, value?]
		// If we're in the room we started in, we can switch to the other start room.
		// Go through a door.
		// Only set a certain number of doors, to discourage easy puzzles.
		dorCount = 0;
		for (let i = 0; i < dorTypes.length; i++) {
			if (dorTypes[i] < 0) { dorCount += 1; }
		}
		//
		for (let i = 0; i < dorr.length; i++) {
			let d = dorr[i];
			if ((d[0] == room) || (d[1] == room)) {
				let dType = dorTypes[i];
				let dest = d[1]==room?d[0]:d[1];
				if (dType == 0 && dorCount <= 3) {
					for (let j = 0; j < dor.length; j++) { // Pick a type for this door.
						let ja = j+1;
						if (doorStat[j] == 1) {
							options.push(['dornew', dest, i, ja]); // dest, index, type
						} else {
							options.push(['dornew', dest, i, -ja]);
						}
					}
				} else if ((doorStat[Math.abs(dType)-1] == 0 && dType < 0) ||
					(doorStat[Math.abs(dType)-1] == 1 && dType > 0)) {
					options.push(['dor', dest]);
				}
			}
		}
		// Use a teleporter
		for (let i = 0; i < telr.length; i++) {
			if (telr[i] == room) { // Is the teleporter in this room?
				// Is the teleporter's destination defined?
				if (telTypes[i] != 0) { // 
					for (let j = 0; j < telr.length; j++) {
						if (j != i && telTypes[j] == telTypes[i]) {
							options.push(['tel', j]);
							break;
						}
					}
				} else {
					// If not, pair it with another teleporter, randomly.
					for (let j = 0; j < telr.length; j++) {
						if (j != i && telTypes[j] == 0) {
							options.push(['telnew', i, j]);
						}
					}
				}
				
			}
		}
		// Toggle a switch. Remember, tiles correspond directly to rooms.
		if (tilTypes[room] == 0) { // New tile
			for (let i = 0; i < dor.length; i++) {
				options.push(['tilnew', i+1, room]);
			}
		} else {
			options.push(['til', tilTypes[room]]);
		}
		// Switch to the other start room, if possible.
		if (room==0) {
			options.push(['swr', 2]);
		} else if (room==2) {
			options.push(['swr', 0]);
		}
		//
		// Pick an option. If the option leads to no unexplored states, remove it.
		optionPicked = false;
		let option = null;
		let stateDesc = [telTypes, room, telix, dorTypes, doorStat, tilTypes]; // Raw representation of current state
		let sv = null; // Raw representation of new state
		let backtrack = false;
		while (optionPicked == false) { // Pick an option that hasn't been explored.
			if (options.length == 0) { // If no new paths, backtrack.
				optionPicked = true;
				backtrack = true;
			} else {
				option = rChoice(options);
				let sstr = '';
				// Pick an action
				if (option[0] == 'tel') {
					let newRoom = telr[option[1]];
					sv = [telTypes, newRoom, telix, dorTypes, doorStat, tilTypes];
				} else if (option[0] == 'telnew') {
					let tempTelTypes = telTypes.slice();
					let newRoom = telr[option[2]];
					tempTelTypes[option[1]] = telix; // New teleporter pair
					tempTelTypes[option[2]] = telix;
					sv = [tempTelTypes, newRoom, telix+1, dorTypes, doorStat, tilTypes];
				} else if (option[0] == 'dor') {
					let newRoom = option[1];
					sv = [telTypes, newRoom, telix, dorTypes, doorStat, tilTypes];
				} else if (option[0] == 'dornew') {
					let newRoom = option[1];
					let tempDorTypes = dorTypes.slice();
					tempDorTypes[option[2]] = option[3];
					sv = [telTypes, newRoom, telix, tempDorTypes, doorStat, tilTypes];
				} else if (option[0] == 'til') { // type
					let tempDoorStat = doorStat.slice();
					tempDoorStat[option[1]-1] = (tempDoorStat[option[1]-1]+1)%2;
					sv = [telTypes, room, telix, dorTypes, tempDoorStat, tilTypes];
				} else if (option[0] == 'tilnew') { // type, tile index
					let tempTilTypes = tilTypes.slice();
					let tempDoorStat = doorStat.slice();
					tempTilTypes[option[2]] = option[1];
					tempDoorStat[option[1]-1] = (tempDoorStat[option[1]-1]+1)%2;
					sv = [telTypes, room, telix, dorTypes, tempDoorStat, tempTilTypes];
				} else if (option[0] == 'swr') {
					let newRoom = option[1];
					sv = [telTypes, newRoom, telix, dorTypes, doorStat, tilTypes];
				}
				// Handle the new state
				sstr = sv.join('@');
				if (states[sstr] == undefined) {
					optionPicked = true;
					states[sstr] = [stateDesc, sv]; // PrevState, state
				} else {
					options.splice(options.indexOf(option), 1);
				}
			}
		}
		// 
		if (backtrack) {
			// Get the raw representation of the previous state.
			sv = states[stateDesc.join('@')][0];
		}
		// Enter new state
		telTypes = sv[0];
		room = sv[1];
		telix = sv[2];
		dorTypes = sv[3];
		doorStat = sv[4];
		tilTypes = sv[5];
		// Continue with the loop
	}
	// Randomly initialize all other objects.
	// Randomly pair remaining teleporters
	let u = []; // Unset teleporters
	for (let i = 0; i < tel.length; i++) {
		if (telTypes[i] == 0) {
			u.push(i);
		}
	}
	while (u.length != 0) {
		let i = Math.floor(Math.random()*u.length);
		telTypes[u[i]] = telix;
		u.splice(i,1);
		i = Math.floor(Math.random()*u.length);
		telTypes[u[i]] = telix;
		u.splice(i,1);
		telix += 1;
	}
	// Randomly set all remaining door types.
	for (let i = 0; i < dor.length; i++) {
		if (dorTypes[i] == 0) {
			dorTypes[i] = (Math.floor(Math.random()*dor.length)+1);
			// Non-essential doors start out closed, to reduce trivial puzzles.
			/*if (Math.random() > .5) {
				dorTypes[i] *= -1;
			}*/
		}
	}
	// Randomly set all remaining tiles to a type that matches a door.
	for (let i = 0; i < til.length; i++) {
		if (tilTypes[i] == 0) {
			tilTypes[i] = Math.abs(rChoice(dorTypes));
		}
	}
	// Fill in the object array 
	let objs = new Array(telTypes.length+dorTypes.length+tilTypes.length);
	// Teleporters
	for (let i = 0; i < tel.length; i++) {
		objs[tel[i]] = 'T'+telTypes[i];
	}
	// Tiles
	for (let i = 0; i < til.length; i++) {
		objs[til[i]] = tilTypes[i]+'*'+tilTypes[i];
		// Invert all associated door types
		for (let j = 0; j < dor.length; j++) {
			if (Math.abs(dorTypes[j]) == tilTypes[i]) {
				dorTypes[j] *= -1;
			}
		}
	}
	// Doors
	for (let i = 0; i < dor.length; i++) {
		objs[dor[i]] = dorTypes[i]>0?'+'+dorTypes[i]:''+dorTypes[i];
	}
	return objs; // The object array for this puzzle.
}

// Has a minimum angle, a default angle, and a maximum angle.
// Can turn towards a specific point.
class Turret {
	constructor(loc, base_angle, arc, turnRate, sprite, lightOffset, range, ship) {
		this.loc = loc;
		this.raw_angle = 0;
		this.base_angle = this.angle = this.head_angle = base_angle;
		this.arc = arc;
		this.turnRate = turnRate;
		this.sprite = sprite
		this.ship = ship;
		this.getLocation();
		this.light = new Light(this, lightOffset, range);
		this.eps = ship.eps; // So that the light is linked to the entry points
	}
	getLocation() {
		this.l = this.hl = add(this.ship.l, pt_addAngle(this.loc, this.ship.angle));
	}
	update(objs, target=null) {
		// Get location relative to ship
		this.getLocation();
		// Turn towards the target
		let turning = false;
		let target_angle = target==null ? 0 : ang_norm(ang(sub(target.l, this.l)) - this.base_angle - this.ship.angle);
		if (this.arc != -1) {
			// If target and current position are on same side of arc, turn towards target.
			let goal_dir = ang_dir(0, target_angle);
			if (goal_dir == ang_dir(0, this.raw_angle)) {
				target_angle = ang_turn(0, target_angle, this.arc);
			} else { // Otherwise, turn the other way.
				target_angle = 0 + .01 * goal_dir;
			}
			
		}
		if (this.raw_angle != target_angle) {
			turning = true;
			this.raw_angle = ang_turn(this.raw_angle, target_angle, this.turnRate);
		}
		this.angle = this.head_angle = ang_norm(this.raw_angle + this.base_angle + this.ship.angle);
		//
		if (this.ship.awake) {
			this.light.update(objs);
		}
		//
		return turning; // Let the base ship play a sound if a turret is turning.
	}
	render(ctx) {
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let l = add(this.l, offset);
		this.sprite.draw(ctx, l[0], l[1], this.angle);
		//
		if (this.ship.awake) {
			this.light.render(ctx, offset);
		}
	}
}

class Tanker {
	constructor(loc) {
		this.s = TK_S;
		this.l = loc.slice();

		this.v = [0,0];
		this.max_vel = .5;
		this.acc = .15;
		this.thrusting = false;

		// Point within 70 degrees of the location angle
	 	this.angle = (.5 - Math.random())*Math.PI*140/180 + ang(loc);
		this.head_angle = this.angle;
		this.awake = true;

		this.entryMessage = "The ancient tanker ship is completely silent as you enter. Even the groans of age have long since died down.";
		this.eps = [];
		this.eps.push(new EntryPoint([36-80, 34+120], [2,4], this, 10));
		this.eps.push(new EntryPoint([125-80, 34+120], [2,21], this, 10));
		console.log('Generating tanker puzzle');
		let tkobjs = getTankerPuzzle(TK_TELEPORTERS, TK_TELROOMS, TK_TILES, TK_DOORS, TK_DORROOMS);
		console.log('Done.');
		this.interior = new Roguelike(this, TK_INTERIOR, tkobjs);
		//
		this.setLs();
		this.initTurrets();
		//
		this.towerRotateSound = document.createElement("audio");
		this.towerRotateSound.src = './audio/towerrotate.mp3';
	}

	initTurrets() {
		this.turrets = []
		// Add a central turret at 83, 124
		this.turrets.push(
			new Turret([80-83,190-124], 0, -1, .005, TKMT_S, 23, 250, this)
		);
		// Add the secondary turrets
		this.turrets.push(
			new Turret([80-42,190-92], Math.PI * 1/4, .8, .003, TKST_S, 10, 200, this)
		);
		this.turrets.push(
			new Turret([80-118,190-92], Math.PI * 7/4, .8, .003, TKST_S, 10, 200, this)
		);
		this.turrets.push(
			new Turret([80-52,190-147], Math.PI * 3/4, .8, .003, TKST_S, 10, 200, this)
		);
		this.turrets.push(
			new Turret([80-108,190-147], Math.PI * 5/4, .8, .003, TKST_S, 10, 200, this)
		);
		this.turrets.push(
			new Turret([80-81,190-274], Math.PI, .8, .003, TKST_S, 10, 200, this)
		);
	}

	hackStart() {
		objs.player.pauseSounds();
		pause(this.towerRotateSound);
	}

	render(ctx) {
		let t = this;
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let l = add(t.l, offset);
		t.s.draw(ctx, l[0], l[1], t.angle);
		//
		t.eps.forEach(e=>{
			e.render(ctx, offset);
		});
		//
		this.turrets.forEach(x=>{
			x.render(ctx);
		});
	}

	setLs() {
		this.hl = add(pt(-10, this.angle), this.l);
		this.hps = [];
	}

	update(objs) {
		let t = this;
		//
		let mv = mag(t.v);
		if (mv > t.max_vel) {
			t.v = pt(t.max_vel, ang(t.v));
		} else if (!t.thrusting && mv > t.max_vel/2){
			t.v = mul(t.v, .995);
		}
		t.l = add(t.l, t.v);
		//
		let turning = false;
		let shooting = false;
		t.setLs();
		//
		let aiming = false;
		//
		t.eps.forEach(e=>{
			e.update(objs);
		});
		// Thrust forwards while awake
		if (t.awake) {
			[[18, 373,19],[43, 373,14],[68,377,27],[103,373,14],[124,373,19]].forEach(m=>{
				let c = 1.5;
				let pl = add(t.l, pt_addAngle([m[0]-80+m[2]/2, 190-m[1]], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(-c, t.angle)), .1);
				objs.particles.push(new Particle(pl, pv, m[2]/3));
			});
			t.v = add(t.v, [t.acc*Math.sin(t.angle), t.acc*Math.cos(t.angle)]);
				t.thrusting = true;
		} else {
			t.v = mul(t.v, .995);
		}
		//
		let target = null;
		if (this.awake && mag(sub(t.l, objs.player.l)) < 600) {
			target = objs.player;
		}
		//
		t.hps = [];
		t.turrets.forEach(x=>{
			turning = x.update(objs, target) || turning;
			t.hps.push([x.l, x.sprite.radius]);
		});
		if (!objs.hackTower) {
	 		if (turning) {
				play(this.towerRotateSound);
			} else {
				pause(this.towerRotateSound);
			}
		}
	}
}