var TOWERLIGHTS_TOP = [
	{x: 15, y: 21, w: 2, h: 1, b:5},
	{x: 21, y: 54, w: 2, h: 2, b:5},
	{x: 21, y: 49, w: 1, h: 1, b:5},

	{x: 84-15, y: 21, w: 2, h: 1, b:5},
	{x: 84-21-1, y: 54, w: 2, h: 2, b:5},
	{x: 84-21-1, y: 49, w: 1, h: 1, b:5},

	{x: 39, y: 53, w: 6, h: 1, b:5},
	{x: 38, y: 50, w: 8, h: 1, b:5},

	{x: 42-2, y: 12, w: 4, h: 3, b:10}
];
var TOWERLIGHTS_BOTTOM = [
	{x: 66-12, y: 50, w: 24, h: 2, b:5},
	{x: 66-14, y: 60, w: 28, h: 2, b:5},
	{x: 66-12, y: 70, w: 24, h: 2, b:5}
	
];
var TOWER_HEAD_SPRITE = new Sprite('./images/towerturret.png', TOWERLIGHTS_TOP);
var TOWER_BODY_SPRITE = new Sprite('./images/towerbase.png', TOWERLIGHTS_BOTTOM);
//var TOWER_HEAD_SPRITE_H = new Sprite('./images/towerturret.png', TOWERLIGHTS_TOP, '#CC0000');
//var TOWER_BODY_SPRITE_H = new Sprite('./images/towerbase.png', TOWERLIGHTS_BOTTOM, '#CC0000');

function getTilePuzzle(dim=3, steps=100) {
	let p = [];
	for (let x=0; x<dim; x++) {
		let temp = [];
		for (let y=0; y<dim; y++) {
			let i = y*dim + x + 1;
			temp.push(i==dim**2 ? null : i);
			i += 1;
		}
		p.push(temp);
	}
	//
	let e = [dim-1, dim-1];
	while (steps > 0) {
		let moves = [];
		if (e[0] > 0) {
			moves.push([-1,0]);
		}
		if (e[0] < dim-1) {
			moves.push([1,0]);
		}
		if (e[1] > 0) {
			moves.push([0,-1]);
		}
		if (e[1] < dim-1) {
			moves.push([0,1]);
		}
		let m = moves[Math.floor(Math.random()*moves.length)];
		let t = add(e, m);
		p[e[0]][e[1]] = p[t[0]][t[1]];
		p[t[0]][t[1]] = null;
		e = t;
		steps -= 1;
	}
	while (e[0] < dim-1) {
		let t = [e[0]+1, e[1]];
		p[e[0]][e[1]] = p[t[0]][t[1]];
		p[t[0]][t[1]] = null;
		e[0] += 1;
	}
	while (e[1] < dim-1) {
		let t = [e[0], e[1]+1];
		p[e[0]][e[1]] = p[t[0]][t[1]];
		p[t[0]][t[1]] = null;
		e[1] += 1;
	}
	//
	let objects = [];
	for (let x=0; x<dim; x++) {
		for (let y=0; y<dim; y++) {
			if (p[x][y] != null) {
				objects.push(p[x][y] + "*"+(x+3*y+1));
			}
		}
	}
	for (let i=1; i<9; i++) {
		objects.push("+"+i);
	}
	return objects;
}

const T_INTERIOR = [
	"#########################",
	"##.#..#..#.....#..#..#.##",
	"#.......................#",
	"##.....................##",
	"#.......................#",
	"#.......................#",
	"##.....................##",
	"#.......................#",
	"##.....................##",
	"##.........***.........##",
	"##.........***.........##",
	"##.........**:.........##",
	"##.....................##",
	"#.......................#",
	"#..........#*#..........#",
	"##.........#*#.........##",
	"#..........#*#..........#",
	"#..........#*#..........#",
	"##.........#*#.........##",
	"#......#...#*#...#......#",
	"############*############",
	"#......#...#*#...#......#",
	"##..........X..........##",
	"##.#.#.#.#.#.#.#.#.#.#.##",
	"#########################"
];
const T_OBJS = getTilePuzzle();

class Tower {
	constructor(loc) {
		this.hs = TOWER_HEAD_SPRITE;
		this.bs = TOWER_BODY_SPRITE;
		this.l = loc.slice();
		this.angle = Math.random()*2*Math.PI;
		this.head_angle = ang_norm(this.angle+Math.PI);
		this.setLs();
		this.awake = true;
		
		//
		this.entryMessage = "Displays flash at the edges of the room as the tower maintains its vigil.";
		this.eps = [];
		this.eps.push(new EntryPoint([0,-53], [2,12], this));
		this.interior = new Roguelike(this, T_INTERIOR, T_OBJS);

		//
		this.light = new Light(this, 23, 500);

		this.towerGunSound = document.createElement("audio");
		this.towerGunSound.src = './audio/towergun.mp3';
		this.towerRotateSound = document.createElement("audio");
		this.towerRotateSound.src = './audio/towerrotate.mp3';
	}

	hackStart(objs) {
		objs.player.pauseSounds();
 		pause(this.towerRotateSound);
 	}

	render(ctx) {
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let bl = add(this.tl, offset);
		let hl = add(this.hl, offset);
		this.bs.draw(ctx, bl[0], bl[1], this.angle);
		this.hs.draw(ctx, hl[0], hl[1], this.head_angle);
		//
 		this.eps.forEach(e=>{
 			e.render(ctx, offset);
 		});
 		if (this.awake) {
 			this.light.render(ctx, offset);
 		}
	}

	setLs() {
		this.hl = add(pt(70, this.angle), this.l);
		this.tl = add(pt(-15, this.angle), this.l);
		this.hps = [[this.hl, TOWER_HEAD_SPRITE.radius]];
	}

	update(objs) {
		let t = this;
		let turning = false;
		let shooting = false;
		t.setLs();
		//
		let aiming = false;
		if (this.awake) {
			let p = objs.player;
			let diff = sub(p.l, t.hl);
			let dist = mag(diff);
			if (dist < 500) {
				aiming = true;
				t.head_angle = ang_turn(t.head_angle, ang(diff), .01);
				turning = true;
				if (ang_dist(t.head_angle, ang(diff)) < .1) {
					[-1,1].forEach(m=>{
						//
						let c = 2;
						let pl = add(t.hl, pt_addAngle([m*19, 30], t.head_angle));
						let pv = perturb(pt(c, t.head_angle), .5);
						objs.particles.push(new Particle(pl, pv, 4));
						//
						pv = perturb(pt(c*3, t.head_angle), .3);
						objs.shells.push(new Shell(pl, pv, t.head_angle, t));
					});
					shooting = true;
				}
			}
			this.eps.forEach(x=>{
				x.update(objs);
			});
			this.light.update(objs);
		}
		//
		if (!aiming && t.head_angle != ang_norm(t.angle+Math.PI)) {
			t.head_angle = ang_turn(t.head_angle, ang_norm(t.angle+Math.PI), .01);
			turning = true;
		}
		//
		if (!objs.hackTower) {
			if (turning) {
				play(this.towerRotateSound);
			} else {
				pause(this.towerRotateSound);
			}
			if (shooting) {
				play(this.towerGunSound);
			} else {
				pause(this.towerGunSound);
			}
		}
	}
}